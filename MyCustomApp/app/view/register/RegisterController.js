/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('MyApp.view.register.RegisterController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.register',

    onRegister: function () {

        var form = this.getView();
        //vrednosti
        console.log(this.getView().getValues());

        Ext.Ajax.request({

            defaultHeaders:{
                'Content-Type': 'application/json'
            },

            url: 'http://localhost:8080/register',

            params: Ext.encode(this.getView().getValues()),

            success: function (response, opts) {
                debugger;
                var obj = Ext.decode(response.responseText);
                console.dir(obj);
            },

            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });

        /*
        if (form.validate()) {
            Ext.Msg.alert('Registration Complete', 'You have successfully registered');
        } else {
            Ext.Msg.alert('Registration Failure', 'Please check for form errors and retry.');
        }
        */
    }

});

