Ext.define('MyApp.view.register.Register', {
    extend: 'Ext.form.Panel',
    xtype: 'form-register',
    controller: 'register',
    title: 'Register',

    //user, first, last, email, pass, rpass,

    layout: {

        type: 'vbox',
        align: 'center',
        pack: 'center'
    },

    //style
    bodyPadding: 20,
    scrollable: 'y',
    width: 'auto',
    autoSize: true,
    fullscreen: true,

    items: [{
        xtype: 'textfield',
        label: 'First Name',
        name: 'firstName',
        required: true,
        style: ' margin-top:15%',
    }, {
        xtype: 'textfield',
        label: 'Last Name',
        name: 'lastName',
        required: true,
    }, {
        xtype: 'emailfield',
        label: 'Email',
        name: 'email',
        allowBlank: false,
        required: true,
        validators: 'email'
    },
        {
            xtype: 'textfield',
            label: 'Username',
            name: 'username',
            required: true,
        },
        {
            xtype: 'passwordfield',
            label: 'Password',
            name: 'password',
            allowBlank: false,
            required: true,
        },
        {
            xtype: 'passwordfield',
            label: 'Repeat password',
            name: 'repeatedPassword',
            allowBlank: false,
            required: true,
        },
    ],

    buttons: [
        {
            text: 'Back',
            handler: 'onBack'
        },
        {
            text: 'Register',
            handler: 'onRegister'
        }
    ]

});
