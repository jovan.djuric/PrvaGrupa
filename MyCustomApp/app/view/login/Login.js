Ext.define('MyApp.view.login.Login', {
    extend: 'Ext.form.Panel',
    xtype: 'form-login',
    controller: 'login',
    title: 'Login',


    layout: {
        type: 'vbox',
        allign: 'middle'
    },

    items: [{
        xtype: 'textfield',
        label: 'Username',
        name: 'username',
        required: true
    }, {
        xtype: 'passwordfield',
        label: 'Password',
        name: 'password',
        required: true
    }],

    buttons: [{
        text: 'Register',
        handler: 'test' // dodati handler da ode na register stranu
    }, {
        text: 'Submit',
        handler: 'onSubmit'
    }]

});